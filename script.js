function createSquareDiv() {
    let square = document.createElement("div");
    square.classList.add("square");
    return square;
}

function createRow(n) {
    let row = document.createElement("div");
    row.classList.add("row");
    for (let i = 0; i < n; i++) {
        row.appendChild(createSquareDiv());
    }
    return row;
}

function createGrid(n) {
    let grid = document.createElement("div");
    grid.classList.add("grid");
    for (let i = 0; i < n; i++) {
        grid.appendChild(createRow(n));
    }
    return grid;
}

setSideSize(16);

function setSideSize(n) {
    let grid = document.querySelector("div.grid");

    if (grid) {
        grid.remove();
    }
    document.querySelector("body").appendChild(createGrid(n));
}

function askSideSize() {
    let n = Number(prompt("How much squares do you want per side ?"));
    if (n >= 0) {
        setSideSize(n);
    } else {
        alert("bad value");
    }
}